/*
File: scripts.js
Date: 2/26/2023
*/

//Hamburger function for mobile nav
function menu(){
  var navlinks = document.getElementById("nav-links");
  var menuicon = document.getElementById("icon");
  
  if(navlinks.style.display === "block") {
    navlinks.style.display = "none";
    menuicon.style.color = "#000";
  } else {
    navlinks.style.display = "block";
    menuicon.style.color = "#000";
  }
  
}

//Image slideshow on About us
let imageNumber = 1;
slideshow(imageNumber)

function moveSlides(x){
  slideshow(imageNumber = imageNumber + x);
}

function slideshow(x) {
  let i;
  let slides = document.getElementsByClassName("slideshow-image");
  if(x > slides.length) {
    imageNumber = 1
  }
  if (x < 1) {
    imageNumber = slide.length;
  }
  for(i=0; i < slides.length; i++){
    slides[i].style.display = "none";
  }

  slides[imageNumber-1].style.display = "block";

}